/*
 *  FILE: 		rot5.c
 *  CREATED: 		23 July 2019
 *  AUTHOR: 		Raymond Thomson
 *  CONTACT: 		raymond.thomson76@gmail.com
 *  COPYRIGHT:		Free Use
 */

#define number0 (int)'0' // decimal 48
#define number9 (int)'9' // decimal 57

#include <stdio.h>
#include <unistd.h>

int main (void) {
	char ch;
	char buf[BUFSIZ];
	int tc;

	while ((tc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < tc;i++) {
			ch = buf[i];
			if (ch >= number0 && ch <= number9) ch = (((ch-number0)+5)%10)+number0;
			buf[i] = ch;
		}
		write(1, buf, tc);
	}
	return 0;
}

