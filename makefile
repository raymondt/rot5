# Application : rot5 cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

rot5: $(SRC)rot5.o
	@echo 'Building and linking target: $@'
	$(CC) -o rot5 $(SRC)*.o $(LIBS)

local: rot5 clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./rot5 to run'

install: rot5 
	@echo 'Installing'	
	cp rot5 /usr/local/bin/rot5
	cp $(MAN)rot5 /usr/share/man/man1/rot5.1
	gzip /usr/share/man/man1/rot5.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f rot5
	@echo 'installed, type rot5 to run or man rot5 for the manual'

remove:
	rm -f rot5

uninstall:
	rm -f /usr/local/bin/rot5
	rm -f /usr/share/man/man1/rot5.1.gz
	@echo 'rot5 uninstalled.'

help:
	@echo 'Make options for rot5'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'
	
